angular.module('BAE.translates', ['pascalprecht.translate', 'BAE.translates.loader', 'ngCookies'])

.config(function ($translateProvider) {
	$translateProvider
		.registerAvailableLanguageKeys(['en'], {
			'en': 'en',
			'zh_TW': 'en'
		})
		.useStaticFilesLoader({
			prefix: 'locales/',
			suffix: '.json'
		})
		.preferredLanguage('en')
		.fallbackLanguage('en')
  		.determinePreferredLanguage()
  		.useSanitizeValueStrategy('escapeParameters')
  		.useLocalStorage();
});