angular.module('gsapp.directives', [])

.directive('gsImg', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			height: '@height',
			width: '@width',
			src: '@src'
		},
		template: '<div style="width:{{width}};height:{{height}};background:url(\'{{src}}\');background-size:cover;background-position:center center;"></div>'
	}
})

.directive('gsComment', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			avatar: '@avatar',
			name: '@name',
			comment: '@comment',
		},
		templateUrl: 'templates/row/gs-comment.html',
		link: function(scope, elem, attrs) {
			var comment = angular.element(elem[0].querySelector('.gs-comment-comment'));
			comment.bind('click', function() {
				var self = angular.element(this);
				if (self.hasClass('close')) {
					self.removeClass('close');
				} else {
					self.addClass('close');
				}
			});
		}
	}
})

// 
.directive('gsSwipeTabs', function($ionicGesture, $window) {
	return {
		restrict: 'A',
		require: 'ionTabs',
		link: function(scope, elem, attrs, tabsCtrl) {
			var TABS_MAX_NUMBER = 4;
			var SCREEN_WIDTH = 100;
			var currentSlide = null;
			var leftSlide = null;
			var rightSlide = null;
			var currentSlideIndex = 0;
			var repositioning = false;
			var translatePercent = 0;
			var dragPoints = [];
			var scrollDiv = [];
			var tabs = [];
			var needUpdateScrollDiv = true;

			scope.initialize = function() {
				SCREEN_WIDTH = $window.innerWidth;
				TABS_MAX_NUMBER = tabsCtrl.tabs.length;

            	for (var i = 0; i < tabsCtrl.tabs.length; i++) {
            		var tab = tabsCtrl.tabs[i];
            		tabs.push(tab.navViewName);
            	}
			}

			scope.initialize();

			var onSwipeLeft = function(){
				var target = tabsCtrl.selectedIndex() + 1;
				if(target < tabsCtrl.tabs.length){
					scope.$apply(tabsCtrl.select(target));
				}
			};
			var onSwipeRight = function(){
				var target = tabsCtrl.selectedIndex() - 1;
				if(target >= 0){
					scope.$apply(tabsCtrl.select(target));
				}
			};

			scope.onSlideMove = function (event) {
				if (needUpdateScrollDiv) {
                	currentSlideIndex = tabsCtrl.selectedIndex();
                	scrollDiv[currentSlideIndex] = document.getElementsByName(tabs[currentSlideIndex])[0];
                	needUpdateScrollDiv = false;
            		currentSlide = angular.element(scrollDiv[currentSlideIndex]);
            		leftSlide = angular.element(scrollDiv[currentSlideIndex - 1]);
            		rightSlide = angular.element(scrollDiv[currentSlideIndex + 1]);
            		if (leftSlide != null) { leftSlide.attr('nav-view', 'active'); }
            		if (rightSlide != null) { rightSlide.attr('nav-view', 'active'); }
				}

                var delta = event.gesture.deltaX;
                translatePercent = delta / SCREEN_WIDTH * 100 * 0.8;

				currentSlide.css('-webkit-transform', 'translate3d(' + translatePercent + '%, 0px, 0px)');
				currentSlide.css('transform', 'translate3d(' + translatePercent + '%, 0px, 0px)');

				if (leftSlide != null) {
					leftSlide.css('-webkit-transform', 'translate3d(' + (translatePercent - 100) + '%, 0px, 0px)');
					leftSlide.css('transform', 'translate3d(' + (translatePercent - 100) + '%, 0px, 0px)');
				}

				if (rightSlide != null) {
					rightSlide.css('-webkit-transform', 'translate3d(' + (translatePercent + 100) + '%, 0px, 0px)');
					rightSlide.css('transform', 'translate3d(' + (translatePercent + 100) + '%, 0px, 0px)');
				}

		        dragPoints.push({
		          t: Date.now(),
		          x: delta
		        });
            };

            scope.onRelease = function (event) {
            	if (dragPoints.length <= 1) { return; }

            	var now = Date.now();
        		var startDrag = dragPoints[dragPoints.length - 1];
        		var releaseX = event.gesture.deltaX;

		        for (var x = dragPoints.length - 2; x >= 0; x--) {
					if (now - startDrag.t > 200) {
						break;
					}
					startDrag = dragPoints[x];
		        }
        		var velocity = Math.abs(startDrag.x - releaseX) / (now - startDrag.t);
        		var isChangePage = (velocity > 0.2 || Math.abs(releaseX) > (SCREEN_WIDTH / 2));

    			if (isChangePage && releaseX < 0 && currentSlideIndex < tabs.length - 1) { // To the Right
    				currentSlideIndex++;
    				leftSlide = currentSlide;
    				currentSlide = rightSlide;
    				rightSlide = null;
    				translatePercent += 100;
    			} else if (isChangePage && releaseX > 0 && currentSlideIndex > 0) { // To the Left
    				currentSlideIndex--;
    				rightSlide = currentSlide;
    				currentSlide = leftSlide;
    				leftSlide = null;
    				translatePercent -= 100;
    			}

            	repositioning = true;
            	var releaseInterval = setInterval(function() {
            		translatePercent = parseInt(translatePercent * 0.72);
					if (translatePercent >= -1 && translatePercent <= 1) {
						translatePercent = 0;
						currentSlide.css('-webkit-transform', 'translate3d(0%, 0px, 0px)');
						currentSlide.css('transform', 'translate3d(0%, 0px, 0px)');

						if (leftSlide != null) {
							leftSlide.css('-webkit-transform', 'translate3d(-100%, 0px, 0px)');
							leftSlide.css('transform', 'translate3d(-100%, 0px, 0px)');
						}

						if (rightSlide != null) {
							rightSlide.css('-webkit-transform', 'translate3d(100%, 0px, 0px)');
							rightSlide.css('transform', 'translate3d(100%, 0px, 0px)');
						}

						repositioning = false;
						scope.$apply(tabsCtrl.select(currentSlideIndex));
						clearInterval(releaseInterval);
					} else {
						currentSlide.css('-webkit-transform', 'translate3d(' + translatePercent + '%, 0px, 0px)');
						currentSlide.css('transform', 'translate3d(' + translatePercent + '%, 0px, 0px)');

						if (leftSlide != null) {
							leftSlide.css('-webkit-transform', 'translate3d(' + (translatePercent - 100) + '%, 0px, 0px)');
							leftSlide.css('transform', 'translate3d(' + (translatePercent - 100) + '%, 0px, 0px)');
						}

						if (rightSlide != null) {
							rightSlide.css('-webkit-transform', 'translate3d(' + (translatePercent + 100) + '%, 0px, 0px)');
							rightSlide.css('transform', 'translate3d(' + (translatePercent + 100) + '%, 0px, 0px)');
						}
					}
            	}, 16);

            	dragPoints = [];
            }
		    
		    //var swipeGesture = $ionicGesture.on('swipeleft', onSwipeLeft, elem).on('swiperight', onSwipeRight);
		    var swipeGesture = $ionicGesture.on('dragleft', scope.onSlideMove, elem)
	    									.on('dragright', scope.onSlideMove, elem)
	    									.on('release', scope.onRelease, elem);
		    scope.$on('$destroy', function() {
		        $ionicGesture.off(swipeGesture, 'dragleft', scope.onSlideMove);
		        $ionicGesture.off(swipeGesture, 'dragright', scope.onSlideMove);
		        $ionicGesture.off(swipeGesture, 'release', scope.onRelease);
		    });

		    scope.$on('tabSelected', function() {
		    	for (var i = 0; i < tabs.length; i++) {
            		var slide = angular.element(scrollDiv[i]);
            		if (slide != null) {
			    		slide.css('-webkit-transform', 'translate3d(0%, 0px, 0px)');
						slide.css('transform', 'translate3d(0%, 0px, 0px)');
						slide.attr('nav-view', 'cached');
            		}
		    	}
				
		    	needUpdateScrollDiv = true;
		    });
		}
	}
});