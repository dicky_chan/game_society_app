angular.module('gsapp.controllers', [])

.controller('TabCtrl', function($scope, $location) {
    $scope.init = function() {
        
    }

    $scope.init();
})

.controller('DashCtrl', function($scope, EventService) {
    $scope.nextGathering = {
        month: 'June',
        date: 21,
        time: '3:00 PM - 11:00 PM',
        venue: 'Room 2306'
    };
    $scope.upcomingEventsList = [];

    $scope.init = function() {
        EventService.GetNextGathering().then(function(response) {
            if (response.status == 200) {
            }
        });
    }

    $scope.init();
})

.controller('GameCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $window, GameService) {
    $scope.gamesList = [];

    $scope.init = function() {
        $ionicLoading.show();
        GameService.GetGamesList().then(function(response) {
            if (response.status == 200) {
                $scope.gamesList = response.data;
            }
        }).finally(function() {
            $ionicLoading.hide();
        });
    };

    $scope.refreshGameList = function() {
        GameService.GetGamesList().then(function(response) {
            if (response.status == 200) {
                $scope.gamesList = response.data;
            }
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.goGameDetail = function(game_id) {
        $ionicLoading.show({
            template: 'Loading...'
        }).then(function () {
            var game = GameService.GetGameDetailFromStorage(game_id);
            if (game != null) {
                GameService.SetCurrentGameDetail(game);
                $state.go('tab.game', {game_id: game._id});
                $ionicLoading.hide();
            } else {
                GameService.GetGameDetail(game_id).then(function(response) {
                    if (response.status == 200) {
                        var game = response.data;
                        GameService.PushGamesList(game);
                        GameService.SetCurrentGameDetail(game);
                        $state.go('tab.game', {game_id: game_id});
                    } else {
                        $window.alert('Unable to get game details. Please try again later');
                    }
                }).finally(function() {
                    $ionicLoading.hide();
                });
            }
        });
    }

    $scope.goGameDetailFake = function() {
        $ionicLoading.show({
            template: 'Loading...'
        }).then(function () {
            $state.go('tab.game', {game_id: 99999});
            $ionicLoading.hide();
        });
    }

    $scope.init();
})

.controller('GameDetailCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $window, GameService) {
    $scope.game = {
        _id:'',
        engName: '',
        chiName: '',
        engDesc: '',
        chiDesc: '',
        copies: 0,
        author: '',
        releaseYear: 1800,
        minPlayers: 0,
        maxPlayers: 0,
        photoUrl: ''
    }

    $scope.init = function() {
        $scope.game = GameService.GetCurrentGameDetail();
    };

    $scope.init();
})

.controller('ChatsCtrl', function($scope) {

})

.controller('ChatDetailCtrl', function($scope, $stateParams) {

})

.controller('AccountCtrl', function($scope, AccountService) {
    $scope.settings = {
        enableFriends: true
    };
    $scope.isLogined = false;
    $scope.user = {
        login_id: "",
        user_uid: "",
        name: ""
    };

    return {
        init: function() {
            if (localStorage['isLogined']) {
                $scope.isLogined = true;
                $scope.user.login_id = localStorage['user_login_id'];
                $scope.user.user_uid = localStorage['user_user_uid'];
                $scope.user.name = localStorage['user_name'];
            } else {
                $scope.isLogined = false;
            }
        },

        login: function(login_id, password) {
            AccountService.Login(login_id, md5(password)).then(function(response) {
                $scope.user.user_uid = response.userID;
                $scope.isLogined = true;
                localStorage['user_user_uid'] = $scope.user.user_uid;
                localStorage['isLogined'] = true;

                localStorage['_session'] = response.session;
            });
        },

        logout: function() {
            $scope.isLogined = false;
        }
    }
})

.controller('RegisterCtrl', function($scope, $window, $ionicLoading, AccountService) {
    $scope.login_id = "";
    $scope.password = "";
    $scope.confirmed_password = "";
    $scope.phone = "";

    $scope.register = function() {
        $ionicLoading.show({
            template: 'Loading...'
        }).then(function () {
            // Form validation
            if ($scope.login_id == null || $scope.login_id.length == 0) {
                $window.alert('Please input username.');
                return;
            }

            if ($scope.password == null || $scope.password.length == 0) {
                $window.alert('Please input password.');
                return;
            }
            
            if ($scope.confirmed_password == null || $scope.confirmed_password.length == 0) {
                $window.alert('Please input Re-enter password.');
                return;
            }
            
            if ($scope.phone == null || $scope.phone.length == 0) {
                $window.alert('Please input phone.');
                return;
            }

            if ($scope.password != $scope.confirmed_password) {
                $window.alert('Password and re-enter password do not match.');
                return;
            }
        }).then(function () {
            $ionicLoading.hide();
        });
        
    }
});
