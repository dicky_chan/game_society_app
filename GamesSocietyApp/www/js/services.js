angular.module('gsapp.services', [])
.factory('ConfigService', function() {
    return {
        SERVER_URL: 'https://gs.oz-workshop.com'
    }
})

.factory('UtilService', function($http, $httpParamSerializer, ConfigService) {
    return {
        GetRequest: function(api, data) {
            // input makeup
            if (api[0] != '/') {
                api = '/' + api;
            }

            var $request = $http({
                method: 'GET',
                url: ConfigService.SERVER_URL + api,
                params: data
            });
            return $request.then(
                function SuccessCallback(response) {
                    return response;
                }
                , function ErrorCallback(response) {
                    return response;
                }
            );
        },


        PostRequest: function(api, data) {
            // input makeup
            if (api[0] != '/') {
                api = '/' + api;
            }

            var $request = $http({
                method: 'POST',
                url: ConfigService.SERVER_URL + api,
                data: $httpParamSerializer(data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            return $request.then(
                function SuccessCallback(response) {
                    return response;
                }
                , function ErrorCallback(response) {
                    return response;
                }
            );
        }
    }
})

.factory('GameService', function(UtilService) {
    var gamesList = [];
    var currentGameDetail = {
        _id: '',
        engName: '',
        chiName: '',
        engDesc: '',
        chiDesc: '',
        copies: 0,
        author: '',
        releaseYear: 1800,
        minPlayers: 0,
        maxPlayers: 0,
        photoUrl: ''
    }

    return {
        GetGamesList: function() {
            return UtilService.GetRequest('/games', null);
        },

        GetGameDetail: function(game_id) {
            return UtilService.GetRequest('/game?', {_id: game_id});
        },

        GetGameDetailFromStorage: function(game_id) {
            for (var i = 0; i < gamesList.length; i++) {
                if (gamesList[i]._id == game_id) {
                    return gamesList[i];
                }
            }

            return null;
        },

        GetCurrentGameDetail: function() {
            return currentGameDetail;
        },

        SetCurrentGameDetail: function(game) {
            currentGameDetail = game;
        },

        ResetCurrentGameDetail: function() {
            currentGameDetail = {
                _id: '',
                engName: '',
                chiName: '',
                engDesc: '',
                chiDesc: '',
                copies: 0,
                author: '',
                releaseYear: 1800,
                minPlayers: 0,
                maxPlayers: 0,
                photoUrl: ''
            }
        },

        PushGamesList: function(game) {
            gamesList.push(game);
        },

        ClearGamesList: function() {
            gamesList = [];
        }
    }

})

.factory('ChatService', function() {
    return {}
})

.factory('AccountService', function(UtilService) {
    return {
        Login: function(login_id, md5_password) {
            var data = {
                username: login_id,
                password: md5_password
            }
            return UtilService.PostRequest('/login', data);
        },

        Register: function(login_id, md5_password) {
            var data = {
                username: login_id,
                password: md5_password
            }
            return UtilService.PostRequest('/login', data);
        }
    }
})

.factory('EventService', function(UtilService) {
    return {
        GetNextGathering: function() {
            return UtilService.GetRequest('/gathering', null);
        },

        GetUpcomingEvents: function() {
            return;
            /*
            var $request = $http({
                method: 'GET',
                url: ConfigServices.SERVER_URL + '/gatherings'
            });
            return $request.then(function(response) {
                return response;
            });
            */
        }
    }
})

.factory('UserService', function() {
    return {
        GetUserInfo: function(user_uid, session) {
            var data = {
                session: session,
                userID: user_uid
            }
            var $request = $http({
                method: 'GET',
                url: ConfigService.SERVER_URL + '/user',
                data: data
            });
            return $request.then(function(response) {
                return response;
            });
        },
    }
})

.factory('HelperService', function($translate) {
    return {
        IntToMonth: function(i) {
            switch (i) {
                case 1: return $translate
            }
        }   
    }
});
